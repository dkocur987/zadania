package com.dkocur.zadania;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewTaskActivity extends AppCompatActivity {

    private EditText etDate;
    private EditText etTime;
    private EditText etTitle;
    private EditText etDescription;
    private Button btnSave;

    public static final String REPLY_DATE = "com.dkocur.zadania.DATE";
    public static final String REPLY_TIME = "com.dkocur.zadania.TIME";
    public static final String REPLY_TITLE = "com.dkocur.zadania.TITLE";
    public static final String REPLY_DESCRIPTION = "com.dkocur.zadania.DESCRIPTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);

        etDate = (EditText) findViewById(R.id.etDate);
        etTime = (EditText) findViewById(R.id.etTime);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etDescription = (EditText) findViewById(R.id.etDescription);
        btnSave = (Button) findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(etDate.getText().toString())
                        || TextUtils.isEmpty(etTime.getText().toString())
                        || TextUtils.isEmpty(etTitle.getText().toString())
                        || TextUtils.isEmpty(etDescription.getText().toString())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String date = etDate.getText().toString();
                    String time = etTime.getText().toString();
                    String title = etTitle.getText().toString();
                    String description = etDescription.getText().toString();
                    replyIntent.putExtra(REPLY_DATE, date);
                    replyIntent.putExtra(REPLY_TIME, time);
                    replyIntent.putExtra(REPLY_TITLE, title);
                    replyIntent.putExtra(REPLY_DESCRIPTION, description);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
