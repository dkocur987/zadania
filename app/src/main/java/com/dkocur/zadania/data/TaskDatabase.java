package com.dkocur.zadania.data;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.dkocur.zadania.data.dao.TaskDao;
import com.dkocur.zadania.data.entity.Task;

@Database(entities = {Task.class}, version = 1)
public abstract class TaskDatabase extends RoomDatabase {

    public abstract TaskDao taskDao();

    private static volatile TaskDatabase INSTANCE;

    public static TaskDatabase getDatabase(final Context context) {
        if(INSTANCE == null) {
            synchronized (TaskDatabase.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(
                            context.getApplicationContext(),
                            TaskDatabase.class,
                            "task_database"
                            ).addCallback(sTaskDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static TaskDatabase.Callback sTaskDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final TaskDao mDao;

        PopulateDbAsync(TaskDatabase db) {
            mDao = db.taskDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            mDao.deleteAll();
            Task task = new Task("2019-01-17", "08:00", "Śniadanie", "Zrobić śniadanie", false);
            mDao.insert(task);
            task = new Task("2019-01-17", "10:00", "Spotkanie Virtual", "Spotkanie firmowe w Rzeszowie", false);
            mDao.insert(task);
            task = new Task("2019-01-17", "12:00", "Obiad", "Spotkanie firmowe w Rzeszowie", false);
            mDao.insert(task);
            task = new Task("2019-01-17", "13:00", "Praca", "Spotkanie firmowe w Rzeszowie", false);
            mDao.insert(task);
            task = new Task("2019-01-17", "14:00", "Ćwiczenia", "Spotkanie firmowe w Rzeszowie", false);
            mDao.insert(task);
            task = new Task("2019-01-17", "15:00", "Odpoczynek", "Spotkanie firmowe w Rzeszowie", false);
            mDao.insert(task);
            return null;
        }
    }
}
